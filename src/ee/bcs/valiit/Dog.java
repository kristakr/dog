package ee.bcs.valiit;

public class Dog extends LivingThing {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void bark (){
        System.out.println(name + " tegi Auh-auh! ja ise on " + getType());
    }

}
