package ee.bcs.valiit;

public class Main {
    public static final String BIG_CONSTANT = "Flash";

	// write your code here
    public static void main(String[] args) {
        Dog Paula=new Dog();
        Dog koer1 = new Dog();
        koer1.setType("Mammal");
        koer1.setName("Muki");
        Dog koer2 = new Dog();
        koer2.setName("Muri");
        koer2.setType("Lind");
        koer1.bark();
        koer2.bark();
        koer2.setName(koer1.getName());
        koer2.bark();
        Paula.bark();

    }
}

